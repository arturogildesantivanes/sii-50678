//////////////////////////////////////////////////////////////////////
#include <fstream>

#include "glut.h"
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#define MAX 200

#include "MundoServidor.h"

//char *org;
/*void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}*/

void* hilo_comandosjugador1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}

void* hilo_comandosjugador2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}

void* hilo_comandosconexiones(void* d)
{
      CMundo* pconexiones=(CMundo*) d;
      pconexiones->GestionaConexiones();
}

CMundo::CMundo()
{
	Init();
	tub=open("/tmp/TuberiaLogger", O_WRONLY);
	
}

CMundo::~CMundo()
{
	write(tub,"F",sizeof("F"));
//Se produce el cierre de la tuberia
	close (tub);
	unlink("/tmp/TuberiaLogger");

//Se produce el cierre de los sockets

	//socketconexcliente.Close();
	//socketcomuncliente.Close();

	acabar=1;
	pthread_join(thjugador1,NULL);
	pthread_join(thjugador2,NULL);
	pthread_join(thconex,NULL);
	
	for(int i=0;i<conexiones.size();i++)
		{ conexiones[i].Close(); }
}


void CMundo::InitGL()
{

	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);

	void GestionaConexiones();
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);




	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5f;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		if(puntos2==3) {exit(1);}

		char cadena[MAX];
		sprintf(cadena, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		write (tub, cadena, strlen(cadena)+1);


	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5f;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		if(puntos1==3) {exit(1);}



		char cadena[MAX];
		sprintf(cadena, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		write (tub, cadena, strlen(cadena)+1);
	}

	/*punterodatos->esfera=esfera;
	punterodatos->raqueta1=jugador1;
		switch (punterodatos->accion)
		{
			case 1: { OnKeyboardDown('w',0,0);
				break;
				}
			case 0: break;
			case -1: { OnKeyboardDown('s',0,0);
				break;
				}
		}*/

		/*char cad2[200];
		sprintf(cad2,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
		socketcomuncliente.Send(cad2,sizeof(cad2));*/

		sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

		 for (i=conexiones.size()-1; i>=0; i--) {
			 if (conexiones[i].Send(cad,200) <= 0) {
			    conexiones.erase(conexiones.begin()+i);
			    if (i < 2) // Hay menos de dos clientes 					conectados
				{
			      // Se resetean los puntos a cero
				puntos1=0;
				puntos2=0;
				} 
			}	
		     }

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
	case 'w':jugador1.velocidad.y=4.0f;break;
	case 's':jugador1.velocidad.y=-4.0f;break;
	case 'o':jugador2.velocidad.y=4.0f;break;
	case 'l':jugador2.velocidad.y=-4.0f;break;

	case 's':jugador1.Mueve(-0.2);break;
	case 'w':jugador1.Mueve(0.2);break;
	case 'l':jugador2.Mueve(-0.2);break;
	case 'o':jugador2.Mueve(0.2);break;

	}*/
	
}

void CMundo::Init()
{

//enlace servidor con puerto

	/*socketconexcliente.InitServer((char*)"127.0.0.1",4000);
	socketcomuncliente=socketconexcliente.Accept();
	socketcomuncliente.Receive(nombrecliente,sizeof(nombrecliente));
	std::cout<<nombrecliente<<std::endl;*/

	servidor.InitServer((char*)"127.0.0.1",4000);
	acabar=0;

	pthread_create(&thjugador1, NULL, hilo_comandosjugador1, this);
	pthread_create(&thjugador2, NULL, hilo_comandosjugador2, this);
	pthread_create(&thconex, NULL, hilo_comandosconexiones, this);


	//pthread_create(&thid1, NULL, hilo_comandos, this);

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	puntos1=0;
	puntos2=0;
	
	//Apertura de la tuberia tub1

	//tub1=open("/tmp/TuberiaClienteServidor",O_WRONLY);


	//Apertura de la tuberia tubtecla

	//tubtecla=open("/tmp/TuberiaTeclas",O_RDONLY);

	//HILO

	

//Se crea un fichero del tamano del atributo DatosMemCompartida, se cierra el descriptor y se asigna la direccion

	/*int file=open("/tmp/datos.txt",O_RDWR|O_CREAT|O_TRUNC,0666);
	write(file,&datos,sizeof(datos));

	org=(char*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ, MAP_SHARED,file,0);

	close(file);

	punterodatos=(DatosMemCompartida*)org;
	punterodatos->accion=0;*/

}



/*void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cadena4[200];

	    socketcomuncliente.Receive(cadena4,sizeof(cadena4));
           // read(tubtecla, cad, sizeof(cad));
            unsigned char key;
            sscanf(cadena4,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
*/

void CMundo::RecibeComandosJugador1()
{
     while (!acabar) {
            usleep(2000);
            char cadena4[200];

	   //socketcomuncliente.Receive(cadena4,sizeof(cadena4));
           // read(tubtecla, cad, sizeof(cad));



	    if(conexiones.size()>0)
	    conexiones[0].Receive(cadena4,sizeof(cadena4));
            unsigned char key;
            sscanf(cadena4,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            //if(key=='l')jugador2.velocidad.y=-4;
            //if(key=='o')jugador2.velocidad.y=4;
      }
}


void CMundo::RecibeComandosJugador2()
{
     while (!acabar) {
            usleep(2000);
            char cadena4[200];

	   // socketcomuncliente.Receive(cadena4,sizeof(cadena4));
           // read(tubtecla, cad, sizeof(cad));

	    if(conexiones.size()>1)
	    conexiones[1].Receive(cadena4,sizeof(cadena4));
            unsigned char key;
            sscanf(cadena4,"%c",&key);
            //if(key=='s')jugador1.velocidad.y=-4;
            //if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::GestionaConexiones()
{
     while (!acabar) {
            usleep(2000);
     
	    conexiones.push_back(servidor.Accept());
	    conexiones[conexiones.size()-1].Receive(nom,sizeof(nom));
            printf("Cliente %s conectado a Servidor",nom);
      }
}
