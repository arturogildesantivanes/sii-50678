//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include "Socket.h"

#define MAX 200


char *org;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	
}

CMundo::~CMundo()
{
	munmap(org,sizeof(datos));
//Se produce el cierre del socket
	//socketcomunservidor.Close();
	servidor.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
	
	char cad1[MAX];
	//socketcomunservidor.Receive(cad1,sizeof(cad1));
	servidor.Receive(cad1,sizeof(cad1));
	sscanf(cad1,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);


	
	//jugador1.Mueve(0.025f);
	//jugador2.Mueve(0.025f);
	/*jugador1.Mueve();
	jugador2.Mueve();
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);




	if(fondo_izq.Rebota(esfera))
	{
		esfera.radio=0.5f;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		if(puntos2==3) {exit(1);}

		char cadena[MAX];
		sprintf(cadena, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		write (tub, cadena, strlen(cadena)+1);


	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.radio=0.5f;
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		if(puntos1==3) {exit(1);}



		char cadena[MAX];
		sprintf(cadena, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		write (tub, cadena, strlen(cadena)+1);
	}*/

	datos.esfera=esfera;
	datos.raqueta1=jugador2;
	punterodatos->esfera=esfera;
	punterodatos->raqueta1=jugador2;	
		switch (punterodatos->accion)
		
		{
			case 1: { OnKeyboardDown('o',1,1);
				break;
				}
			case 0: break;
			case -1: { OnKeyboardDown('l',1,1);
				break;
				}
		}
	

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[MAX];
	sprintf(cad,"%c",key);
	//socketcomunservidor.Send(cad,sizeof(cad));
	servidor.Send(cad,sizeof(cad));

	/*switch(key)
	{
	case 'w':jugador1.velocidad.x=0.1;break;
	case 's':jugador1.velocidad.x=-0.1;break;
	case 'o':jugador2.velocidad.x=0.1;break;
	case 'l':jugador2.velocidad.x=-0.1;break;

	case 's':jugador1.Mueve(-0.2);break;
	case 'w':jugador1.Mueve(0.2);break;
	case 'l':jugador2.Mueve(-0.2);break;
	case 'o':jugador2.Mueve(0.2);break;

	}*/

	
}


void CMundo::Init()
{


//conexion del cliente al puerto

	printf("Nombre del Cliente:\n");
	char nombre[200];
	//gets(nombre);
	std::cin>>nombre;
	//socketcomunservidor.Connect((char*)"127.0.0.1",4000);
	//socketcomunservidor.Send(nombre,sizeof(nombre));
	servidor.Connect((char*)"127.0.0.1",4000);
	servidor.Send(nombre,sizeof(nombre));


//printf("%s",nombre);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	puntos1=0;
	puntos2=0;
//Se crea un fichero del tamano del atributo DatosMemCompartida, se cierra el descriptor y se asigna la direccion

	datos.esfera=esfera;
	datos.raqueta1=jugador2;

	file=open("/tmp/datos.txt",O_RDWR|O_CREAT,0666);
	write(file,&datos,sizeof(datos));
	if (fstat(file, &bstat)<0) {
	perror("Error en fstat del fichero"); 
	close(file);
	}

	org=(char*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ, MAP_SHARED,file,0);

	close(file);

	punterodatos=(DatosMemCompartida*)org;
	punterodatos->accion=0;

}
